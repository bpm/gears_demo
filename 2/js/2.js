
var GearsDemo = {

    // config stuff
    permit_json_url: "/get_permits.json",
    stylesheet_url_prefix: '/stylesheets/',
    stylesheets: ['create_permit_link.xml',
		  'create_permit_form.xml'],
    checkNetworkURL: '/online.html',

    // util for making HTTP requests
    callWithContent: function(url, fn) {
	new Ajax.Request(url, {
		onSuccess:function(transport) {
		    fn(transport.responseText);
		},
		onFailure: function(transport) {
		    alert('something went wrong while trying to access the page '+url);
		}
	    });
    },

    // setting up the db
    db: google.gears.factory.create('beta.database'),

    initDB: function () {
	if (window.google && google.gears) {
	    GearsDemo.db.open('GearsDemo');

	    // permit data
	    GearsDemo.db.execute('create table if not exists permits' +
				 '('+
				 ' id INTEGER PRIMARY KEY,'+
				 ' xml TEXT(320),'+
				 ' outdated'+
				 ')')

	    // stylesheets
	    GearsDemo.db.execute('create table if not exists stylesheets' +
				 '('+
				 ' name TEXT(25) PRIMARY KEY,'+
				 ' xml TEXT(320)'+
				 ')')
	} else {
	    alert('you need to install google gears!');
	}
    },

    // getting the stylesheets from the server
    callWithStylesheets: function (fn) {
	GearsDemo.stylesheets.each(function(stylesheet) {
		GearsDemo.callWithContent(GearsDemo.stylesheet_url_prefix+stylesheet, function(xml) {
			fn(stylesheet, xml);
		    });
	    });
    },

    // putting the stylesheets in the DB
    downloadStylesheets: function() {
	GearsDemo.callWithStylesheets(function(name, xml) {
		GearsDemo.db.execute('insert or replace into stylesheets values (?,?)',
				     [name, xml]);
	    });
    },

    // getting individual stylesheets form the DB
    getStylesheet: function(name) {
	var rt = GearsDemo.db.execute('select xml from stylesheets where name=?', [name]);
	return rt.field(0);
    },

    // getting permits from the serer
    callWithPermits: function(fn) {
	GearsDemo.callWithContent(GearsDemo.permit_json_url,
				  function(string) { fn( string.evalJSON().permits) });
    },

    // clearing the permit DB
    //    clearPermitDB: function() {
    //	GearsDemo.db.execute('delete from permits where 1');
    //    },

    //	putting the permits in the db
    downloadAndRefreshPermits: function() {
	GearsDemo.callWithPermits(function(permits) {
		GearsDemo.db.execute('begin;');
		GearsDemo.db.execute('delete from permits where 1');
		permits.each(function(permit) {
			GearsDemo.db.execute('insert into permits(id,xml,outdated) values (?,?,0)',
					     [permit.id, permit.xml]);
		    });
	        GearsDemo.db.execute('commit;');
		alert('downloading permits from the server');
		GearsDemo.syncPermitIndex();
	    });
    },

    // testing the contents of the db (for debugging)
    alertPermitRows: function () {
	var rs = GearsDemo.db.execute('select * from permits order by id');
	while (rs.isValidRow()) {
	    alert(rs.field(0) + ' ' + rs.field(1) + ' ' + rs.field(2));
	    rs.next();
	}
    },

    // getting permit XML
    getPermitXML: function(id) {
	return GearsDemo.db.execute('select xml from permits where id=?', [id]).field(0);
    },

    // updating permits
    updatePermit: function(id, xml) {
	GearsDemo.db.execute('begin');
	GearsDemo.db.execute('delete from permits where id=?', [id])
	GearsDemo.db.execute('insert into permits (id,xml,outdated) values (?, ?, 1)', [id,xml])
	GearsDemo.db.execute('commit');
	alert('updating permit '+id+': '+xml);
    },

    // has this permit been changed by the user?
    isPermitUpdated: function(permitid) {
	return GearsDemo.db.execute('select outdated from permits where id=?', [permitid]).field(0);
    },

    // has anything been changed by the user?
    isAnythingChanged: function() {
	return GearsDemo.db.execute('select * from permits where outdated=1').field(0);
    },

    // iterating over the permits
    mapPermits: function(fn) {
	var rs = GearsDemo.db.execute('select * from permits order by id');
	while (rs.isValidRow()) {
	    fn(rs.fieldByName('id'), rs.fieldByName('xml'), rs.fieldByName('outdated'));
	    rs.next();
	}
    },

    // create the HTML for the list of permits
    createPermitIndex: function() {
	var entries = [];
	var stylesheet = xmlParse(GearsDemo.getStylesheet('create_permit_link.xml'));
	GearsDemo.mapPermits(function(id,xml) {
	  entries.push(xsltProcess(xmlParse(xml), stylesheet));
	});
	return entries.length ? '<ul>'+entries.join('')+'</ul>': '<p>You have not downloaded any permits from the server yet</p>';
    },

    // sync permit index
    syncPermitIndex: function() {
	$('permits').innerHTML = GearsDemo.createPermitIndex();
    },

    // statly variable for locking menu
    permitIndexLocked: null,
    
    // observe permit index
    observePermitIndex: function() {
      $('permits').observe('click', function(event) {
	      if (!GearsDemo.permitIndexLocked) {
		  var el = event.element();
		  if (el.hasClassName('permit_index_clickable')) {
		      el.innerHTML = xsltProcess(xmlParse(GearsDemo.getPermitXML(el.siblings()[0].innerHTML)),
						 xmlParse(GearsDemo.getStylesheet('create_permit_form.xml')));
		      el.removeClassName('permit_index_clickable');
		      GearsDemo.permitIndexLocked = true;
		  }
	      };
	  });
    },

    // uploading changes
    uploadChanges: function() {
	var rs = GearsDemo.db.execute('select * from permits where outdated=1 order by id');
	while (rs.isValidRow()) {
	    alert('Sending new xml for permit '+rs.field(0)+' to the server: '+rs.field(1));
	    rs.next();
	}
	GearsDemo.downloadAndRefreshPermits();
    },

    // activate "upload changes" button
    observeUploadChangesButton: function() {
	$('upload_changes_button').observe('click', GearsDemo.uploadChanges)
    },

    // activate "refresh permits" button
    observeRefreshPermitsButton: function() {
        Event.observe('refresh_permits_button', 'mouseup', GearsDemo.downloadAndRefreshPermits);
    },

    // creating update permit XML from permit forms, pt 1
    updatePermitTemplate: new Template('<permit outdated="true"><id>#{permit_id}</id><name>#{permit_name}</name><address>#{permit_address}</address></permit>'),

    // updating permits from permit forms, pt 2
    updatePermitFromForm: function(serialized_form) {
	GearsDemo.updatePermit(serialized_form.permit_id,
			       GearsDemo.updatePermitTemplate.evaluate(serialized_form))
    },

    // activate permit forms (what happens when you click on "update permit")
    observePermitForms: function() {
	Event.observe('permits','submit', function(event) {
		var el = event.element();
		if (el.hasClassName('submit_update_permit_form')) {
		    var permit_id = Form.serialize(el, true).permit_id;
		    var form_id = 'update_permit_'+permit_id;
		    var form_element = $(form_id);
		    var serialized_form = Form.serialize($(form_element), true);
		    GearsDemo.updatePermitFromForm(serialized_form);
		};
	    });
    },

    // check network function
    checkNetworkStatus: function() {
	$('network_status').innerHTML = '<b><span style="color: red">offline</span></b>';
	$('refresh_permits_button').disable();
	$('upload_changes_button').disable();
	new Ajax.Request(GearsDemo.checkNetworkURL, {
		asynchronous: null,
		onSuccess: function() {
		$('refresh_permits_button').enable();
		if (GearsDemo.isAnythingChanged()) {
		    $('upload_changes_button').enable();
		};
		$('network_status').innerHTML = '<b>online</b>' ;
		}
	});
    },
    
    // displaying the network status
    observeNetworkStatus: function() {
	new PeriodicalExecuter(GearsDemo.checkNetworkStatus, 5);
    },

    // activate local store
    activateLocalStore: function() {
	var localServer = google.gears.factory.create('beta.localserver');
	var store = localServer.createManagedStore('test-store');
	store.manifestUrl = 'site-manifest.txt';
	store.checkForUpdate();
    }

}
