
var GearsDemo = {

    // config stuff
    permit_xml_url: "/get_permits.xml",
    permit_json_url: "/get_permits.json",

    stylesheet_url_prefix: '/stylesheets/',

    stylesheets: ['create_permit_link.xml'],

    // util for making HTTP requests
    callWithContent: function(url, fn) {
	new Ajax.Request(url, {
		onSuccess:function(transport) {
		    fn(transport.responseText);
		},
		onFailure: function(transport) {
		    alert('something went wrong while trying to access the page '+url);
		}
	    });
    },

    // setting up the db
    db: google.gears.factory.create('beta.database'),

    initDB: function () {
	if (window.google && google.gears) {
	    GearsDemo.db.open('GearsDemo');

	    // permit data
	    GearsDemo.db.execute('create table if not exists permits' +
				 '('+
				 ' id INTEGER PRIMARY KEY,'+
				 ' xml TEXT(320),'+
				 ' outdated'+
				 ')')

	    // stylesheets
	    GearsDemo.db.execute('create table if not exists stylesheets' +
				 '('+
				 ' name TEXT(25) PRIMARY KEY,'+
				 ' xml TEXT(320)'+
				 ')')
	} else {
	    alert('you need to install google gears!');
	}
    },

    // testing the db
    fillDB: function () {
	alert('filling db!!!');
	GearsDemo.db.execute("begin;"+
"insert or replace into permits values (1, '<permit1/>', 0);"+
"insert or replace into permits values (2, '<permit2/>', 0);"+
"insert or replace into permits values (3, '<permit3/>', 0);"+
"commit;")

    },

    alertPermitRows: function () {
	var rs = GearsDemo.db.execute('select * from permits order by id');
	while (rs.isValidRow()) {
	    alert(rs.field(0) + ' ' + rs.field(1) + ' ' + rs.field(2));
	    rs.next();
	}
		  
    },

    // getting the stylesheets from the server
    callWithStylesheets: function (fn) {
	GearsDemo.stylesheets.each(function(stylesheet) {
		GearsDemo.callWithContent(GearsDemo.stylesheet_url_prefix+stylesheet, function(xml) {
			fn(stylesheet, xml);
		    });
	    });
    },

    // putting the stylesheets in the DB
    syncStylesheets: function() {
	GearsDemo.callWithStylesheets(function(name, xml) {
		GearsDemo.db.execute('insert or replace into stylesheets values (?,?)',
				     [name, xml]);
	    });
    },

    // getting individual stylesheets form the DB
    getStylesheet: function(name) {
	var rt = null;
	rt = GearsDemo.db.execute('select xml from stylesheets where name=?', [name]);
	return rt.field(0);
    },

    // getting permits from the serer
    callWithPermits: function(fn) {
	GearsDemo.callWithContent(GearsDemo.permit_json_url,
				  function(string) { alert(1); fn( string.evalJSON().permits) });
    },

    // clearing the permit DB
    clearPermitDB: function() {
	GearsDemo.db.execute('delete from permits where 1');
    },

    //	putting the permits in the db
    updatePermits: function() {
	GearsDemo.callWithPermits(function(permits) {
		GearsDemo.clearPermitDB();
		GearsDemo.db.execute('begin;');
		permits.each(function(permit) {
			GearsDemo.db.execute('insert into permits(id,xml,outdated) values (?,?,0)',
					     [permit.id, permit.xml]);
		    });
	        GearsDemo.db.execute('commit;');
		GearsDemo.alertPermitRows();
	    });
    },

    // getting permit XML
    getPermitXML: function(id) {
	return GearsDemo.db.execute('select xml from permits where id=?', [id]).field(0);
    },

    // updating permits
    updatePermit: function(id, xml) {
	GearsDemo.db.execute('begin');
	GearsDemo.db.execute('delete from permits where id=?', [id])
	GearsDemo.db.execute('insert into permits (id,xml,outdated) values (?, ?, 1)', [id,xml])
    },

    // has this permit been changed by the user?
    isPermitUpdated: function(permitid) {
	return GearsDemo.db.execute('select outdated from permits where id=?', [permitid]).field(0);
    },

    // iterating over the permits
    mapPermits: function(fn) {
	var rs = GearsDemo.db.execute('select * from permits order by id');
	while (rs.isValidRow()) {
	    fn(rs.fieldByName('id'), rs.fieldByName('xml'), rs.fieldByName('outdated'));
	    rs.next();
	}
    }

}