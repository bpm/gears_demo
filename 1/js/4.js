
var GearsDemo = {

    // config stuff
    permit_xml_url: "/get_permits.xml",

    stylesheet_prefix: '/stylesheets/',

    stylesheets: ['create_insert_permits_js.xml'],

    // util for making HTTP requests
    callWithContent: function(url, fn) {
	new Ajax.Request(url, {
		onSuccess:function(transport) {
		    fn(transport.responseText);
		},
		onFailure: function(transport) {
		    alert('something went wrong while trying to access the page '+url);
		}
	    });
    },

    // setting up the db
    db: google.gears.factory.create('beta.database'),

    initDB: function () {
	if (window.google && google.gears) {
	    GearsDemo.db.open('GearsDemo');

	    // permit data
	    GearsDemo.db.execute('create table if not exists permits' +
				 '('+
				 ' id INTEGER PRIMARY KEY,'+
				 ' xml TEXT(320),'+
				 ' outdated'+
				 ')')

	    // stylesheets
	    GearsDemo.db.execute('create table if not exists stylesheets' +
				 '('+
				 ' name TEXT(25) PRIMARY KEY,'+
				 ' xml TEXT(320)'+
				 ')')
	} else {
	    alert('you need to install google gears!');
	}
    },

    // testing the db
    fillDB: function () {
	alert('filling db!!!');
	GearsDemo.db.execute("begin;"+
"insert or replace into permits values (1, '<permit1/>', 0);"+
"insert or replace into permits values (2, '<permit2/>', 0);"+
"insert or replace into permits values (3, '<permit3/>', 0);"+
"commit;")

    },

    alertPermitRows: function () {
	var rs = GearsDemo.db.execute('select * from permits order by id');
	while (rs.isValidRow()) {
	    alert(rs.field(0) + ' ' + rs.field(1) + ' ' + rs.field(2));
	    rs.next();
	}
		  
    },

    // getting the stylesheets from the server
    callWithStylesheets: function (fn) {
	GearsDemo.stylesheets.each(function(stylesheet) {
		GearsDemo.callWithContent(GearsDemo.stylesheet_prefix+stylesheet, function(xml) {
			fn(stylesheet, xml);
		    });
	    });
    },

    // putting the stylesheets in the DB
    syncStylesheets: function() {
	GearsDemo.callWithStylesheets(function(name, xml) {
		GearsDemo.db.execute('insert or replace into stylesheets values (?,?)',
				     [name, xml]);
	    });
    },

    // getting individual stylesheets form the DB
    getStylesheet: function(name) {
	var rt = null;
	rt = GearsDemo.db.execute('select xml from stylesheets where name=?', [name]);
	return rt.field(0);
    },

    // getting permit XML from the serer
    callWithPermitXML: function(fn) {
	GearsDemo.callWithContent(GearsDemo.permit_xml_url, fn);
    },

    // clearing the permit DB
    clearPermitDB: function() {
	GearsDemo.db.execute('delete from permits where 1');
    },

    // creating permit SQL
    callWithInsertPermitSQL: function(fn) {
	GearsDemo.callWithPermitXML(function(xml) {
		var sql = xsltProcess(xmlParse(xml),
				      xmlParse(GearsDemo.getStylesheet('create_insert_permits_sql.xml')));
		fn(sql);
	    });
    },

    // creating the insert-a-permit-form
    // !!! sorry for this hack !!!
    callWithInsertPermitJS: function(fn) {
	GearsDemo.callWithPermitXML(function(xml) {
		var sql = xsltProcess(xmlParse(xml),
				      xmlParse(GearsDemo.getStylesheet('create_insert_permits_js.xml')));
		fn(sql);
	    });
    },
	
    //	putting the permits in the db
    uploadPermits: function() {
	GearsDemo.callWithInsertPermitSQL(function(sql) {
		alert('sql: ' + sql);
		GearsDemo.clearPermitDB();
		alert(1);
		//		GearsDemo.db.execute(sql, []);
		GearsDemo.db.execute('begin;');
		GearsDemo.db.execute("insert into permits (id,xml,outdated) values (1,1,0);");
		GearsDemo.db.execute("insert into permits (id,xml,outdated) values (2,1,0);");
		GearsDemo.db.execute('commit;');

		alert(2);
		GearsDemo.alertPermitRows();
		alert(3);
	    });
    }

}