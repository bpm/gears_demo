
var GearsDemo = {

    db: null,

    initDB: function () {
	if (window.google && google.gears) {
	    try {
		GearsDemo.db = google.gears.factory.create('GearsDemo');

		if (GearsDemo.db) {
		    GearsDemo.db.open('GearsDemo');
		    GearsDemo.db.execute('create table if not exists permits' +
					  '('+
					  ' id INTEGER PRIMARY KEY,'+
					  ' xml TEXT(320),'+
					  ' outdated'+
					  ')')
			}
	    } catch (ex) {
		alert('could not create Google Gears database: ' + ex.message);
	    }
	}
    },

    fillDB: function () {
	GearsDemo.db.execute('insert into permits values (1, <permit/>, 0)')
    },

    getPermitRows: function () {
	GearsDemo.db.execute('select * from permits order by id');
    }

}


