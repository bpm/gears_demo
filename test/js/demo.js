
var PermitDemo = {

    getPermits_URL: '_get_permits.xml',

    permitDBURL: '_permit_db/',

    db: null,

    initDB: function () {
	if (window.google && google.gears) {
	    try {
		PermitDemo.db = google.gears.factory.create('permit_demo');

		if (PermitDemo.db) {
		    PermitDemo.db.open('permit_demo');
		    PermitDemo.db.execute('create table if not exists permits' +
					  '('+
					  ' id INTEGER PRIMARY KEY,'+
					  ' xml TEXT(320),'
					  ' outdated'
					  ')')
			}
	    } catch (ex) {
		alert('could not create Google Gears database: ' + ex.message);
	    }
	}
    },

    syncPermit: function(id, xml, outdated) {
	PermitDemo.db.execute('insert or replace into permits (?, ?, ?)',
			      [id, xml, outdated]);
    };

    syncPermits: function() {
	

	    

	    


    getPermits: function() {
	
    
    